#ifndef QTDREPORTCHATREQUEST_H
#define QTDREPORTCHATREQUEST_H

#include <QObject>
#include "common/qabstracttdobject.h"

class QTdReportChatRequest : public QTdObject
{
    Q_OBJECT
    Q_DISABLE_COPY(QTdReportChatRequest)
public:
    explicit QTdReportChatRequest(QObject *parent = nullptr);
};

#endif // QTDREPORTCHATREQUEST_H
